const html2canvas = require("html2canvas");

const enableScreenshot = () => {
  document.onvisibilitychange = () => {
    if (document.visibilityState === "hidden") {
      html2canvas(document.body, { backgroundColor: "#000000" }).then(
        (canvas) => {
          const base64image = canvas.toDataURL("image/png");
          localStorage.setItem("screenshot", base64image);
        }
      );
    }
  };
};

module.exports = {
  enableScreenshot,
};
